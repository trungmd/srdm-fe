export function isAuthenticated() {
    // Kiểm tra xem người dùng đã đăng nhập hay chưa
    // Trả về true nếu đã đăng nhập, ngược lại trả về false
    const user = localStorage.getItem('user');
    return !!user;
}

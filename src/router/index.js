import AuthLayout from '@/layout/AuthLayout.vue';
import MainLayout from '@/layout/MainLayout.vue';
import LoginViewAuthen from '@/view/authen/LoginView.vue';
import CourseListVue from '@/view/course/CourseList.vue';
import DashboardListVue from '@/view/dashboard/DashboardList.vue';
import ManagerListVue from '@/view/manager/ManagerList.vue';
import StudentDetail from '@/view/student/StudentDetail.vue';
import StudentListVue from '@/view/student/StudentList.vue';
import SyllabusListVue from '@/view/syllabus/SyllabusList.vue';
import TeacherDetail from '@/view/teacher/TeacherDetail.vue';
import TeacherListVue from '@/view/teacher/TeacherList.vue';
import ForgotPassword from '@/view/authen/ForgotPassword.vue';
import ClassroomListVue from '@/view/classroom/ClassroomList.vue';
import SubjectListVue from '@/view/subject/SubjectList.vue';
import LessonViewTableRowAndColumVue from '@/view/lesson/LessonViewTableRowAndColum.vue';
import RoleListVue from '@/view/role/RoleList.vue';
import calendarList from '@/view/lesson/calendarList.vue';

// const permission = JSON.parse(localStorage.getItem('permission'));

export const routes = [
    {
        path: '/',
        component: AuthLayout,
        children: [
            {
                path: 'login',
                component: LoginViewAuthen,
                name: 'login',
            },
            {
                path: 'forgot-password',
                component: ForgotPassword,
                name: 'forgot-password',
            },
        ],
    },
    {
        path: '/',
        component: MainLayout,
        meta: { requiresAuth: true },
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: DashboardListVue,
            },
            {
                path: '/student',
                component: StudentListVue,
            },
            {
                path: '/student/:id',
                component: StudentDetail,
                name: 'student-detail',
            },
            { path: '/teacher', component: TeacherListVue },
            {
                path: '/teacher/:id',
                component: TeacherDetail,
                name: 'teacher-detail',
            },
            { path: '/manager', component: ManagerListVue },
            { path: '/course', component: CourseListVue },
            {
                path: '/syllabus',
                name: 'syllabus',

                component: SyllabusListVue,
            },
            {
                path: '/classroom',
                component: ClassroomListVue,
            },
            {
                path: '/subject',
                component: SubjectListVue,
            },
            {
                path: '/lesson',
                component: LessonViewTableRowAndColumVue,
            },
            {
                path: '/role',
                component: RoleListVue,
            },
            {
                path: '/calendar',
                component: calendarList,
            },
        ],
    },
    // { path: '/', component: DashboardListVue },
];
